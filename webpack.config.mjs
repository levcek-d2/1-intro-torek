import HtmlWebpackPlugin from "html-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import { resolve, dirname } from "path";
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));
export default (_env, _argv) => ({
  entry: {
    "main": "./src/main"
  },
  module: {
    rules: [
      {
        test: /\.coffee$/,
        use: {
          loader: "coffee-loader"
        }
      },
      {
				test: /\.svelte$/,
				use: {
					loader: "svelte-loader",
					options: {
            compilerOptions: {
              dev: true
            },
            emitCss: false,
            hotReload: true
          }
        }
			},
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Katanc",
      templateContent: ({htmlWebpackPlugin}) =>
        `<!DOCTYPE html>
        <html>
          <head>
            <meta charset="utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>${htmlWebpackPlugin.options.title}</title>
            <!-- <link rel="stylesheet" href="index.css" /> -->
            <!-- <script type="module" src='main.js'></script> -->
          </head>
          <body>
          </body>
        </html>`,
      chunks: ["main"],
      filename: "index.html"
    }),
    new CopyPlugin({
      patterns: [
        {
          from: resolve(__dirname, "./introductions"),
          to: resolve(__dirname, "./public")
        }
      ]
    })
  ],
  resolve: {
    extensions: ["*", ".js", ".jsx"],
    alias: {
      "introductions": resolve(__dirname, "./data/introductions/index.js"),
      "ui-core": resolve(__dirname, "./src/ui-core/index.js"),
      "ui-html": resolve(__dirname, "./src/ui-html/index.js"),
      "ui-svg": resolve(__dirname, "./src/ui-svg/index.js"),
      "utils": resolve(__dirname, "./src/utils/index.js"),

      
      "katanc-model": resolve(__dirname, "../katanc-model/src/index.js"),
      "katanc-svg": resolve(__dirname, "../katanc-svg/src/index.js"),
      "katanc-config": resolve(__dirname, "../katanc-config/src/index.js"),
      "katanc-utils": resolve(__dirname, "../katanc-utils/src/index.js"),
      "katanc": resolve(__dirname, "../katanc/src/index.js")
    }
  },
  output: {
    path: resolve(__dirname, "./public"),
    filename: "[name].js",
  },
  devServer: {
    static: resolve(__dirname, "./public"),
  }
});