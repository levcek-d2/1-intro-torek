import Name from "./Name.svelte";
import { participants } from "../introductions";

Object.assign(window, { participants })

new Name({
    target: document.body,
    props: {
        name: "Lev"
    }
});

