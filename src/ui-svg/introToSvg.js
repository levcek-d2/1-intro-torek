import opentype from "opentype.js";
import SPC from "svg-path-commander";
const { ceil, round } = Math; 
async function introToSvg(introduction) {
    const { name, color } = introduction;
    const [
        bBox,
        pathString,
        pathLength
    ] = await new Promise((resolve, reject) => 
    opentype.load("./Roboto-Black.ttf", (err, font) => {
        if (err) {
            reject(err);
        } else {
            if (!name) { return; };
            const path = font.getPath(name, 0, 0, 12);
            const pString = path.toPathData();
            resolve([
                SPC.getPathBBox(pString),
                pString,
                SPC.getTotalLength(pString)
            ])
        };
    }));
    return { 
        id: name,
        width: bBox.width,
        height: bBox.height,
        viewBox: `${bBox.x} ${bBox.y} ${bBox.width} ${bBox.height}`,
        path: {
            d: pathString,
            length: pathLength,
            fill: "transparent",
            stroke: color,
            strokeWidth: 0.1,
            strokeDasharray: ceil(pathLength),
            strokeDashoffset: 0
        },
        string: `<svg
            id="${name}"
            xmlns="http://www.w3.org/2000/svg"
            width="${bBox.width * 10}"
            height="${bBox.height * 10}"
            viewBox="${bBox.x} ${bBox.y} ${bBox.width} ${bBox.height}">
            <path
                d="${pathString}"
                stroke="${color}"
                stroke-width="0.1"
                stroke-dasharray="${ceil(pathLength)}"
                stroke-dashoffset="0"
                fill="transparent" />
        </svg>`,
        length: SPC.getTotalLength(pathString)
    };
};
export default introToSvg;