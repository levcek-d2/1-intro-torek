function draw(node) {
    return {
        duration: 5000,
        tick: (t) => {
            const current = Number(node.getAttribute("stroke-dashoffset"));
            node.setAttribute("stroke-dashoffset", current - 1);
        }
    };
};
export default draw;